package resources.utilities;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;

public class ReadExcel {


    @DataProvider(name = "emailTestData")
    public Object[][] getData(Method m) throws IOException {

        String excelSheetName = m.getName();

        File file = new File(System.getProperty("user.dir")+"\\src\\test\\java\\testdata\\testdataemail.xlsx");
        FileInputStream fis = new FileInputStream(file);
        Workbook wb = WorkbookFactory.create(fis);
        Sheet sheetName = wb.getSheet(excelSheetName);

        int totalRows = sheetName.getLastRowNum();

        Row rowCells = sheetName.getRow(0);
        int totalCols = rowCells.getLastCellNum();

        String[][] testData = new String[totalRows][totalCols];
        for (int i=1; i<=totalRows;i++){
            for (int j=0; j<totalCols;j++){
                testData[i-1][j] = String.valueOf(sheetName.getRow(i).getCell(j));
                System.out.println(testData[i-1][j]);
            }
        }
        return testData;
    }


}
