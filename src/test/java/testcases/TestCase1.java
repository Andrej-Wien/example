package testcases;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import resources.utilities.ReadExcel;
import setup.SetupClass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TestCase1 extends SetupClass {


    @Test(dataProviderClass = ReadExcel.class, dataProvider = "emailTestData", priority = 1)
    public static void email(String emailData) throws InterruptedException, IOException {

        SetupClass.setUp();

        WebElement contactUs = driver.findElement(By.xpath(locators.getProperty("contactUs")));
        Actions actions = new Actions(driver);
        actions.moveToElement(contactUs);
        actions.perform();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"wt-cli-accept-all-btn\"]")).click();
        contactUs.click();

        WebElement name = driver.findElement(By.id(locators.getProperty("name")));
        name.sendKeys("Andrej");

        WebElement email = driver.findElement(By.id(locators.getProperty("email")));
        email.sendKeys(emailData);

        WebElement mobile = driver.findElement(By.id(locators.getProperty("mobile")));
        mobile.sendKeys("+38976313999");

        WebElement subject = driver.findElement(By.id(locators.getProperty("subject")));
        subject.sendKeys("testAutomation");

        WebElement yourMsg = driver.findElement(By.id(locators.getProperty("yourMsg")));
        yourMsg.sendKeys("Test msg lorem ipsum bla bla bla");

        WebElement sendBtn = driver.findElement(By.xpath(locators.getProperty("sendBtn")));
        sendBtn.click();

        Thread.sleep(2000);
        WebElement errorMsg = driver.findElement(By.className("wpcf7-not-valid-tip"));
        Assert.assertTrue(errorMsg.isDisplayed());
        System.out.println(errorMsg.getText());

    }

    @Test(priority = 2)
    public static void secondTestCase() throws InterruptedException, IOException {

        SetupClass.setUp();

        Thread.sleep(2000);
        WebElement companyBtn = driver.findElement(By.xpath(locators.getProperty("companyLink")));
        companyBtn.click();
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.musala.com/company/");

        Thread.sleep(1000);
        WebElement leadershipSection = driver.findElement(By.xpath(locators.getProperty("leaderSection")));
        Actions actions = new Actions(driver);
        actions.moveToElement(leadershipSection);
        actions.perform();
        Thread.sleep(1000);
        Assert.assertTrue(leadershipSection.isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"wt-cli-accept-all-btn\"]")).click();

        String mainWindowHandle = driver.getWindowHandle();
        Set<String> windowHandles = driver.getWindowHandles();
        System.out.println("before clicking " + windowHandles.size());
        driver.findElement(By.xpath(locators.getProperty("fbLink"))).click();
        windowHandles = driver.getWindowHandles();
        Thread.sleep(1000);
        System.out.println("after clicking " + windowHandles.size());

        for (String w : windowHandles) {
            if (!w.equals(mainWindowHandle)) {
                driver.switchTo().window(w);//switch to fb window

                Assert.assertEquals(driver.getCurrentUrl(), "https://www.facebook.com/MusalaSoft?fref=ts");
                Thread.sleep(1000);
                driver.findElement(By.xpath("//*[@id=\"facebook\"]/body/div[2]/div[1]/div/div[2]/div/div/div/div[2]/div/div[2]/div[1]/div/div[1]/div/span/span")).click();
                Thread.sleep(2000);
                //problems with xpath but selected another el
                WebElement logoPic = driver.findElement(By.xpath(locators.getProperty("fbLogoPic")));
                Thread.sleep(2000);
                Assert.assertTrue(logoPic.isDisplayed());

            }
        }
    }

    @Test(priority = 3)
    public static void thirdCase() throws InterruptedException, IOException {
        SetupClass.setUp();

        Thread.sleep(3000);
        WebElement careersBtn = driver.findElement(By.xpath(locators.getProperty("careersBtn")));
        careersBtn.click();
        driver.findElement(By.id("wt-cli-accept-all-btn")).click();

        WebElement checkOurPositions = driver.findElement(By.xpath(locators.getProperty("checkPositions")));
        checkOurPositions.click();
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.musala.com/careers/join-us/");

        Thread.sleep(1000);
        WebElement ddown = driver.findElement(By.name("get_location"));
        Select select = new Select(ddown);
        select.selectByValue("Macedonia");

        driver.findElement(By.xpath("/html/body/main/div/div/section/div[2]/article[2]/div/a/div/div[1]")).click();
        Thread.sleep(1000);

        WebElement sectionOne = driver.findElement(By.xpath(locators.getProperty("jobSectionOne")));
        WebElement sectionTwo = driver.findElement(By.xpath(locators.getProperty("jobSectionTwo")));
        WebElement section3 = driver.findElement(By.xpath(locators.getProperty("jobSectionThree")));
        WebElement section4 = driver.findElement(By.xpath(locators.getProperty("jobSectionFour")));

        List<Object> acctualSections = new ArrayList<>();
        acctualSections.add(sectionOne.getText());
        acctualSections.add(sectionTwo.getText());
        acctualSections.add(section3.getText());
        acctualSections.add(section4.getText());

        List<Object> expectedSections = new ArrayList<>();
        expectedSections.add("General description");
        expectedSections.add("Requirements");
        expectedSections.add("Responsibilities");
        expectedSections.add("What we offer");

        for (int i = 0; i < acctualSections.size(); i++) {
            // System.out.println("Actual: " + acctualSections.get(i) + " Expected : " + expectedSections.get(i));
            Assert.assertTrue(acctualSections.get(i).equals(expectedSections.get(i)));
        }

        Thread.sleep(2000);
        WebElement applyBtn = driver.findElement(By.xpath(locators.getProperty("applyBtn")));
        Thread.sleep(1000);

        Assert.assertTrue(applyBtn.isDisplayed());

        Thread.sleep(2000);
        applyBtn.click();

        applyBtn.sendKeys(Keys.chord(Keys.CONTROL, "0"));
        Thread.sleep(1000);
        WebElement applyName = driver.findElement(By.xpath(locators.getProperty("applyName")));
        applyName.sendKeys("andrej");

        WebElement applyEmail = driver.findElement(By.xpath(locators.getProperty("applyEmail")));

        WebElement applyMobile = driver.findElement(By.xpath(locators.getProperty("applyMobile")));
        applyMobile.sendKeys("+38976313999");

        String filePath = "C:\\Users\\Vanessa Marija\\IdeaProjects\\musala\\src\\test\\java\\resources\\images\\testpic.png";
        Thread.sleep(2000);
        driver.findElement(By.xpath("//input[@id='uploadtextfield']")).sendKeys(filePath);
        Thread.sleep(2000);
        WebElement linkedIn = driver.findElement(By.xpath(locators.getProperty("linkedIn")));
        linkedIn.sendKeys("www.linkedin.com/in/andrejmiskovski\n");

        WebElement msgArea = driver.findElement(By.xpath(locators.getProperty("msgArea")));
        msgArea.sendKeys("Lorem ipsum skjdlah 1234");

        WebElement iAgreeCheckbox = driver.findElement(By.xpath(locators.getProperty("iAgreeCheckbox")));
        iAgreeCheckbox.click();

        WebElement applySendBtn = driver.findElement(By.xpath(locators.getProperty("applySendBtn")));

        int caseId;
        for (int i = 0; i <= 2; i++) {
            caseId = i;
            switch (caseId) {
                case 0:
                    applyEmail.sendKeys("123abc");
                    applySendBtn.click();
                    Thread.sleep(3000);
                    WebElement closeErrorBtn = driver.findElement(By.xpath("//*[@id=\"wpcf7-f880-o1\"]/form/div[5]/div/button"));
                    Thread.sleep(3000);
                    WebElement errorMsg = driver.findElement(By.xpath("//*[@id=\"wpcf7-f880-o1\"]/form/div[5]/div/div"));
                    Assert.assertTrue(errorMsg.isDisplayed());
                    closeErrorBtn.click();
                case 1:
                    Thread.sleep(1000);
                    applyEmail.clear();
                    applyEmail.sendKeys("123@");
                    applySendBtn.click();
                    WebElement closeErrorBtn1 = driver.findElement(By.xpath("//*[@id=\"wpcf7-f880-o1\"]/form/div[5]/div/button"));
                    Thread.sleep(3000);
                    WebElement errorMsg1 = driver.findElement(By.xpath("//*[@id=\"wpcf7-f880-o1\"]/form/div[5]/div/div"));
                    Assert.assertTrue(errorMsg1.isDisplayed());
                    closeErrorBtn1.click();
                    break;
                case 2:
                    applyEmail.clear();
                    applyEmail.sendKeys(" ");
                    applySendBtn.click();
                    WebElement closeErrorBtn2 = driver.findElement(By.xpath("//*[@id=\"wpcf7-f880-o1\"]/form/div[5]/div/button"));
                    Thread.sleep(3000);
                    WebElement errorMsg2 = driver.findElement(By.xpath("//*[@id=\"wpcf7-f880-o1\"]/form/div[5]/div/div"));
                    Assert.assertTrue(errorMsg2.isDisplayed());
                    closeErrorBtn2.click();
                    break;
            }
        }

    }

    @Test(priority = 4)
    public static void fourthCase() throws IOException, InterruptedException {
        SetupClass.setUp();

        WebElement careersBtn = driver.findElement(By.xpath(locators.getProperty("careersBtn")));
        careersBtn.click();

        driver.findElement(By.id("wt-cli-accept-all-btn")).click();

        WebElement checkOurPositions = driver.findElement(By.xpath(locators.getProperty("checkPositions")));
        checkOurPositions.click();

        WebElement ddown = driver.findElement(By.name("get_location"));
        Select select = new Select(ddown);
        select.selectByValue("Sofia");
        Thread.sleep(2000);
        List<WebElement> jobs = driver.findElements(By.tagName("article"));

        for (int j = 0; j < 1; j++) {
            List<WebElement> getCity = driver.findElements(By.className("card-jobsHot__location"));
            System.out.println(getCity.get(j).getText());
            System.out.println("------");
        }
        for (int i = 0; i < jobs.size(); i++) {
            if (jobs.get(i).getText().contains("Sofia")) {
                // String sofia = jobs.get(i).getText();
                List<WebElement> getJobDescription = driver.findElements(By.className("card-jobsHot__title"));
                System.out.println("Position: " + getJobDescription.get(i).getText());

                List<WebElement> moreInfo = driver.findElements(By.className("card-jobsHot__link"));
                System.out.println("More info : " + moreInfo.get(i).getAttribute("href"));

                System.out.println("-----------------------------");
            }
        }


    }


}


