package setup;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SetupClass {

    public static WebDriver driver;
    public static Properties prop = new Properties();
    public static Properties locators = new Properties();


    public static void setUp() throws IOException {

        if (driver == null) {
            FileReader fr = new FileReader(System.getProperty("user.dir") + "\\src\\test\\java\\resources\\configfiles\\config.properties");
            FileReader frLocators = new FileReader(System.getProperty("user.dir") + "\\src\\test\\java\\resources\\configfiles\\config.locators");
            prop.load(fr);
            locators.load(frLocators);

        }

        if (prop.getProperty("browser").equalsIgnoreCase("chrome")){
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.get(prop.getProperty("testurl"));
            driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
            driver.manage().window().maximize();

        } else if (prop.getProperty("browser").equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
            driver.get(prop.getProperty("testurl"));
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        }

    }


    @AfterMethod(enabled = false)
    public void end(){
        driver.close();
        System.out.println("closed successful");
    }

}
